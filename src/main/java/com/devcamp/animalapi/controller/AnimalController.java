package com.devcamp.animalapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.model.Cat;
import com.devcamp.animalapi.model.Dog;
import com.devcamp.animalapi.service.CatService;
import com.devcamp.animalapi.service.DogService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AnimalController {
    @Autowired
    private CatService catService;
    @Autowired
    private DogService dogService;

    @GetMapping("/cats")
    public ArrayList<Cat> catsList() {
        ArrayList<Cat> cats = catService.allCats();
        return cats;
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> dogsList() {
        ArrayList<Dog> dogs = dogService.allDogs();
        return dogs;
    }

}
