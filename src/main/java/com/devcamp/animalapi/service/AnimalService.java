package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Animal;
import com.devcamp.animalapi.model.Cat;
import com.devcamp.animalapi.model.Dog;

@Service
public class AnimalService {
    Cat cat1 = new Cat("Cash");
    Cat cat2 = new Cat("Money");
    Cat cat3 = new Cat("Moon");
    Dog dog1 = new Dog("Bee");
    Dog dog2 = new Dog("Bun");
    Dog dog3 = new Dog("Mat");

    public ArrayList<Animal> allAnimals() {
        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(cat1);
        animals.add(cat2);
        animals.add(cat3);

        animals.add(dog1);
        animals.add(dog2);
        animals.add(dog3);
        return animals;

    }

}
