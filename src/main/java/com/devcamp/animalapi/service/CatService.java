package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Animal;
import com.devcamp.animalapi.model.Cat;

@Service
public class CatService {
    @Autowired
    private AnimalService animalService;

    public ArrayList<Cat> allCats() {
        ArrayList<Animal> animals = animalService.allAnimals();
        ArrayList<Cat> cats = new ArrayList<>();
        for (Animal animal : animals) {
            if (animal instanceof Cat) {
                cats.add((Cat) animal);
            }
        }
        return cats;
    }

}
