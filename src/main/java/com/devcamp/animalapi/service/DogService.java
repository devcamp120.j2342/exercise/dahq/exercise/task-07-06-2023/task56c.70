package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Animal;
import com.devcamp.animalapi.model.Dog;

@Service
public class DogService {
    @Autowired
    private AnimalService animalService;

    public ArrayList<Dog> allDogs() {
        ArrayList<Animal> animals = animalService.allAnimals();
        ArrayList<Dog> dogs = new ArrayList<>();
        for (Animal animal : animals) {
            if (animal instanceof Dog) {

                dogs.add((Dog) animal);
            }
        }
        return dogs;
    }

}
